<?php include 'header.php';?>
<section class="top-banner">
	<?php include 'nav.php';?>
	<div class="container">
		<div class="mtb80">
			<h1 class="mb-4" style="font-weight: 500 !important;">E-COMMERCE SIMPLIFIED.</h1>
		<h4>Get the freedom to focus on product development and brand management, leave the rest to us.</h4>
		</div>
		<div class="card-deck">
		  <div class="card">
			<img class="card-img-top" src="assets/img/adobestock_138282057.jpg" alt="">
			<div class="card-body">
			  <h6 class="card-title">CHANNEL SETUP AND INTEGRATION</h6>
			  <p class="card-text">JEG offers ready-to-go data exchange integration with over 40 channels and marketplaces.  You could be up and running in days not weeks.</p>
			</div>
		  </div>
		  <div class="card">
			<img class="card-img-top" src="assets/img/adobestock_98392476.jpg" alt="">
			<div class="card-body">
			  <h6 class="card-title">LISTING MANAGEMENT</h6>
			  <p class="card-text">The quality of your content and listings is critical, and JEG has over a decade of experience and proven success listing in all major commerce sites.</p>
			</div>
		  </div>
		  <div class="card">
			<img class="card-img-top" src="assets/img/adobestock_49661779.jpg" alt="">
			<div class="card-body">
			  <h6 class="card-title">FULFILLMENT AND CUSTOMER SUPPORT</h6>
			  <p class="card-text">Integrated 3PL service ensures your orders are shipped promptly and your customers kept happy.</p>
			</div>
		  </div>
		</div>
	</div>
</section>
<section class="bg_gray">
	<div class="container max1002">
	<div class="row">
		<div class="col-md-4">
			<div class="d-flex align-items-center">
				<div class="col-5">
				<h1>40+</h1>
			    </div>
			    <div class="col-7">
				<p class="text-uppercase">Integrated <br>Channels</p>
			</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="d-flex align-items-center ml-white">
				<div class="col-8"><h1>top 50</h1></div>
				<div class="col-4"><p class="text-uppercase">Amazon<br> Seller</p></div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="d-flex align-items-center"><div class="col-6">
				<h1>5000+</h1></div><div class="col-6">
				<p class="text-uppercase">Active <br>Products</p></div>
			</div>
		</div>
	</div>
</div>
</section>
<hr>
<section>
<div class="container no-padding">
	<div class="row row_set d-md-flex align-items-center">
		<div class="col-md-6">
			<div class="text-center p15">
				<h2>YOUR BUSINESS SIMPLIFIED</h2><hr class="heading_hr">
				<p class="max500">We eliminate the need to develop an army of on-line store managers and content developers. Our team will create, manage and broadcast your product catalog to any of over 40 integrated stores and marketplaces We will manage and fulfill your on-line orders, facilitate returns and provide top rated customer service.</p>
				<a class="btn btn-primary mb-4 mt-5" href="#">READ MORE</a>
			</div>
		</div>
		<div class="col-md-6">
			<img class="img-fluid" src="assets/img/laberinto.jpg">
		</div>
	</div>
	<div class="row row_set bg_gray better mb-5">
		<div class="col-md-6">
			<img class="img-fluid2" src="assets/img/shutterstock_439876288.jpg">
		</div>
		<div class="col-md-6">
			<div class="p15">
				<div class="row">		 
					<div class="col-12 col-sm-6">
						<div class="icon_text"><div class="d-flex align-items-start">
							<img src="assets/img/listing.gif" width="45" height="45">
							<div class="icon-left">
								<h6>BETTER LISTINGS</h6>
								<p>Set your product apart from the competition and attract more customers.</p>
							</div>
						</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="icon_text"><div class="d-flex align-items-start">
							<img src="assets/img/setup.gif" width="45" height="45">
							<div class="icon-left">
								<h6>CHANNEL SETUP</h6>
								<p>Get going fast! we already have direct integrations with the top e-commerce sites.</p>
							</div>
						</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="icon_text"><div class="d-flex align-items-start">
							<img src="assets/img/customerservice.gif" width="45" height="45">
							<div class="icon-left">
								<h6>CUSTOMER SERVICE</h6>
								<p>Keep your customers happy and your on-line vendor reputation high.</p>
							</div>
						</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="icon_text"><div class="d-flex align-items-start">
							<img src="assets/img/returns.gif" width="45" height="45">
							<div class="icon-left">
								<h6>RETURNS MANAGEMENT</h6>
								<p>We will issue return authorizations and manage all aspects of the return process.</p>
							</div>
						</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="icon_text mb-0"><div class="d-flex align-items-start">
							<img src="assets/img/shipping.gif" width="45" height="45">
							<div class="icon-left">
								<h6>FAST SHIPPING</h6>
								<p>Exceed your customers expectations and move your products faster.</p>
							</div>
						</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="icon_text mb-0"><div class="d-flex align-items-start">
							<img src="assets/img/accountmanagement.gif" width="45" height="45">
							<div class="icon-left">
								<h6>ACCOUNT MANAGEMENT</h6>
								<p>Our channel experts will help you sell more and save money in the process.</p>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
</section>
<section class="services">
	<div class="container">
		<h2 class="text-white text-center">OUR SERVICES</h2><hr class="heading_hr">
		<section class="bg_gray2">
			<div class="container">
				<div class="row">
					<div class="col-md-4 text-center">
						<div class="service">
							<img class="img-fluid" src="assets/img/jegcommercelogo.png">
						<h6>WORRY-FREE E-COMMERCE</h6>
						<p>Covers all aspects of channel management; account and electronic integration, product listing, product content optimization and cross channel pricing management.</p>
						<a href="#">Read More</a>
						</div>
					</div>
					<div class="col-md-4 text-center">
						<div class="service">
						<img class="img-fluid" src="assets/img/jeg3pllogo.png">
						<h6>INTEGRATED LOGISTICS</h6>
						<p>Our Enhanced 3PL service, ensures optimal product availability, fast order management, prompt shipments and efficient management of returned products.</p>
						<a href="#">Read More</a>
					</div></div>
					<div class="col-md-4 text-center"><div class="service">
						<img class="img-fluid" src="assets/img/jegtrade.png">
						<h6>PRODUCT DISTRIBUTION</h6>
						<p>Leading technology distributor of IT and electronics products, offering end-to-end solutions for today’s resellers and retailers.</p>
						<a href="#">Read More</a></div>
					</div>
				</div>
			</div>
		</section>
	</div>
</section>
<?php include 'footer.php';?>

