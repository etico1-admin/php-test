<?php include 'header.php';?>
<section class="serviceheader">
	<?php include 'nav.php';?>
</section>
<section>
<div class="container">
	<h1 class="text-center">SERVICES</h1>
	<div class="service_page">
		<div class="row">
			<div class="col-md-3">
				
			</div>
			<div class="col-md-9">
				<h2>JEG COMMERCE</h2>
				<hr class="heading_hr heading_hr_blue2">
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<img class="img-fluid" src="assets/img/1.jpg">
			</div>
			<div class="col-md-9">
				<p>JEG’s Commercial Services takes care of all aspects of channel management including: account and electronic integration, product listing, product content optimization and cross channel pricing management.</p>
				<p>Main activities:</p>
				<ul class="has_dot">
					<li>Channel management: Set accounts and transaction integration.</li>
					<li>Product catalog management: Manage product pages in the marketplace.</li>
					<li>Cross-channel inventory management: Allow sales to any channel with one central inventory.</li>
					<li>Cross-channel price management: Eliminate price erosion resulting from automatic price matching.</li>
					<li>Channel marketing tools use and sales optimization: in-channel marketing advice and coordination.</li>
					<li>Product launch coordination: Manage e-commerce product launch schedule.</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="service_page">
		<div class="row">
			<div class="col-md-3">
				
			</div>
			<div class="col-md-9">
				<h2>JEG 3PL</h2>
				<hr class="heading_hr heading_hr_blue3">
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<img class="img-fluid" src="assets/img/2.jpg">
			</div>
			<div class="col-md-9">
				<p>Our Enhanced 3PL service, ensures optimal product availability, fast order management, prompt shipments and efficient management of returned products.</p>
				<p>Main activities:</p>
				<ul class="has_dot">
					<li>In-bound receiving: Receive, inspect for quantity/quality and put away.</li>
					<li>Storage: Storage, counting and moving.</li>
					<li>Product kitting: virtual and physical bundle management, re-packaging and product re-configuration.</li>
					<li>Order fulfillment: Pick, pack, and ship.</li>
					<li>Return management: RMA management, Receiving, inspection and testing. Refurbishing, re-package and downgrade.</li>
					<li>Customer service and refund. Warranty support</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="service_page">
		<div class="row">
			<div class="col-md-3">
				
			</div>
			<div class="col-md-9">
				<h2>JEG TRADE</h2>
				<hr class="heading_hr heading_hr_blue4">
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<img class="img-fluid" src="assets/img/3.jpg">
			</div>
			<div class="col-md-9">
				<p>Our traditional distribution business buys and sells high volume electronics and accessories.</p>
			</div>
		</div>
	</div>
</div>
</section>
<?php include 'footer.php';?>