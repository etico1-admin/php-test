<section class="bg_gray">
	<footer>
	<div class="container">
		<div class="footer-top">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 d-flex align-items-center mb-2 mt-2">
                 <i class="fa fa-envelope-o fa-block"></i>
                 <div class="icon-left">
                 	<p class="mb-0">HAVE ANY QUESTIONS?</p>
                 <p class="contact_info">info@jegsons.com</p>
                 </div>
				</div>
				<div class="col-md-4 d-flex align-items-center mb-2 mt-2">
                 <i class="fa fa-phone fa-block"></i>
                 <div class="icon-left">
                 	<p class="mb-0">CALL US</p>
                 <p class="contact_info">+1 (305) 654 7555</p>
                 </div>
				</div>
				<div class="col-md-4 d-flex align-items-center mb-2 mt-2">
                 <i class="fa fa-building-o fa-block"></i>
                 <div class="icon-left">
                 	<p class="mb-0">VISIT US</p>
                 <p class="contact_info">20000 NE 15th Ct, Miami, FL 33179</p>
                 </div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<h3 class="text-center">Looking for Work? <a href="https://www.paycomonline.net/v4/ats/web.php/jobs?clientkey=9FA3F3DE88A7C32BE15DBF3FDCE47F40" class="text-underline">Apply Today</a></h3></div>
	</div>
	</footer>
</section>
</body>
<script src='https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js'></script>
</html>