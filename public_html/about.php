<?php include 'header.php';?>
<section class="aboutheader">
	<?php include 'nav.php';?>
</section>	
<section>
<div class="container no-padding">
	<div class="row d-md-flex align-items-center">
		<div class="col-md-6">
			<div class="p15">
				<h1 class="text-center">ABOUT JEG</h1>
<hr class="heading_hr">
				
				
				
					
					
					
					

			</div>
		</div>
		
	</div><div class="row d-md-flex align-items-center">
		<div class="col-md-6">
			<div class="p15">
				
				<p>We are disrupting our industry by making it easier for our clients to develop their business and fuel profitability with US e-tailers and retailers.</p>
				<p>JEG allows manufacturers to increase their sales by accessing multiple sales channels, with little or no upfront costs. With JEG managing all aspects of your business, you are free to focus on what makes your company more competitive.</p>
				<p>For the past 10 years JEG has enjoyed tremendous success selling on-line. Our investment in technology and knowhow, allowed us to reach multiple milestones and places us at the very front of the eCommerce industry:</p>
					<p class="mb-0">• Top 50 Seller on Amazon.com.</p>
					<p class="mb-0">• Established commercial relationships with top retailers and sales platforms.</p>
					<p class="mb-0">• State-of-the-Art distribution center, enables us to provide same-day shipping for most products across multiple verticals - direct to your customer's door</p>
					<p class="mb-0">• EDI integration with E-commerce’s largest channels for seamless and reliable order management.</p>

			</div>
		</div>
		<div class="col-md-6">
			<img class="img-fluid" src="assets/img/shutterstock_234372730.jpg">
		</div>
	</div>
</div>	
</section>
<section>
	<div class="container">
		<div class="row d-md-flex align-items-center">
		<div class="col-md-6">
			<img class="img-fluid" src="assets/img/adobestock_123882763.jpg">
		</div>
		<div class="col-md-6">
			<div>
				<h1 class="text-center">WHAT WE DO</h1><hr class="heading_hr">
				<p>If you have a great brand and compelling portfolio of products, the next step is to make sure your consumers can easily find them on-line, buy them with one click and receive them fast. JEG will add world class eCommerce Capabilities to your business without upfront costs.</p>
				<p>JEG’s Commercial Services takes care of all aspects of channel management including: account and electronic integration, product listing, product content optimization and cross channel pricing management.</p>
				<p>Our Enhanced 3PL service, ensures optimal product availability, fast order management, prompt shipments and efficient management of returned products.</p>
				<p>JEG’s integrated channel management and logistics service delivers a smooth continuum of service; from listing to shipping</p>
			</div>
		</div>
		
	</div>
	</div>
</section>
<section>
	<div class="container text-center">
		<h1>E-COMMERCE MADE EASY</h1>
		<img class="img-fluid" src="assets/img/responsibilities2.jpg">
	</div>
</section>
<?php include 'footer.php';?>